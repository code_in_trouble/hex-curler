# Library for cUrl-based games

curling allows you to create simple games served over HTTP, intended to be
interacted with in a CLI environment with curl.

State is stored locally on the user's machine in cookies.

## Basic interaction

    curl -c c.txt -b c.txt http://endpoint.com/
    You have 100HP, 10 coins. What do you want to do?
    >fight
    >heal (-5 coins)
    curl -c c.txt -b c.txt http://endpoint.com/fight
    You won, but took some damage. You find 2 coins!
    You have 75HP, 12 coins.
    >fight
    >heal (-5 coins)
    # edits cookie file to go back to 100HP
    curl -c c.txt -b c.txt http://endpoint.com/fight
    Error: invalid state!
