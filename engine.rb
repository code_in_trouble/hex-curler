class InvalidChecksumError < StandardError
end

class Engine
  def initialize hash
    validate_checksum hash
    hash_to_state hash
  end

  def validate_checksum hash
    if !checksum_valid? hash
      raise InvalidChecksumError
    end
  end

  def checksum_valid? hash
    if hash.empty?
      return true
    end

    expected = checksum hash
    expected == hash['checksum']
  end

  def checksum hash
    string = hash
      .keys
      .filter { |k| k != 'checksum' }
      .sort
      .map { |k| "#{k}:#{hash[k]}" }
      .append("secret:#{secret}")
      .join '|'
    Digest::SHA2.hexdigest string
  end

  def hash_to_state hash
  end

  def step command
  end

  def state_h
    hash = state_to_hash
    hash['checksum'] = checksum hash
    hash
  end

  def state_to_hash
    {}
  end

  def message
  end
end
