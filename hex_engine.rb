require './engine'
require './rooms'

class Hex < Engine
  attr_accessor :state, :health, :endurance, :xp, :level, :keys, :magic_weapon, :magic_armor
  def initialize hash
    super
  end

  def hash_to_state hash
    @state = hash['state'] || 'play'
    @health = (hash['health'] || 6).to_i
    @endurance = (hash['endurance'] || 6).to_i
    @xp = (hash['xp'] || 0).to_i
    @level =( hash['level'] || 1).to_i
    @keys = (hash['keys'] || 0).to_i
    @magic_weapon = (hash['magic_weapon'] || 0).to_i
    @magic_armor = (hash['magic_armor'] || 0).to_i
    @room = str_to_room(hash['room'] || 'welcome').new self
    @messages = []
  end

  def str_to_room name
    case name
    when 'welcome'
      Rooms::Welcome
    when 'store'
      Rooms::Store
    when 'empty'
      Rooms::Empty
    when 'trap'
      Rooms::Trap
    when 'monster'
      Rooms::Monster
    when 'treasure'
      Rooms::Treasure
    when 'stairs'
      Rooms::Stairs
    when 'boss'
      Rooms::Boss
    when 'exit'
      Rooms::Exit
    end
  end

  def step command
    case @state
    when 'play', 'invalid'
      resolve_last_room command
    when 'dead'
      add_message <<~MSG.chomp
        You are dead.
        Thanks for playing! Better luck next time.
      MSG
    when 'win'
      add_message <<~MSG.chomp
        You have slain the monsters and emerge from the keep victorious!
        Congratulations and thanks for playing!
      MSG
    else
    end

    if @state == 'play'
      enter_next_room
    end
  end

  def resolve_last_room command
    @room.resolve command
  end

  def enter_next_room
    roll_room
    @room = get_room
    @room.enter
  end

  def get_room_from_roll
    (case @last_roll + @keys
    when 1, 3
      Rooms::Empty
    when 2
      Rooms::Trap
    when 4, 5
      Rooms::Monster
    when 6
      Rooms::Treasure
    when 7
      Rooms::Stairs
    when 8
      Rooms::Boss
    when 9
      Rooms::Exit
    end).new self
  end

  def get_room
    if @xp >= 50
      Rooms::Store.new self
    else
      get_room_from_roll
    end
  end

  def roll_room
    if @next_roll != nil
      @last_roll = @next_roll
    else
      @last_roll = roll_die
    end
  end

  def roll_die
    1 + rand(6)
  end

  def add_message message
    @messages.push(message)
  end

  def update_hex health, endurance, xp
    @health += health
    @endurance += endurance
    @xp += xp
  end

  def state_to_hash
    hash = {}
    hash['state'] = @state
    hash['health'] = @health
    hash['endurance'] = @endurance
    hash['xp'] = @xp
    hash['level'] = @level
    hash['keys'] = @keys
    hash['magic_weapon'] = @magic_weapon
    hash['magic_armor'] = @magic_armor
    hash['room'] = @room.to_s
    hash
  end

  def message
    <<~MSG
      H: #{@health} E: #{@endurance} X: #{@xp} Keys: #{@keys}
      Weapon: #{@magic_weapon} Armor: #{@magic_armor} Level: #{@level}
      #{@messages.join "\n"}
    MSG
  end
end
